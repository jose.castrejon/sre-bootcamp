# Welcome to SRE Bootcamp 2022

This is a website designed for you! in it youll find all the information you need to get started with the Monitor Plus Cloud Bootcamp



# Meet our app - Pod dashboard

![](static/img/app-micro.gif)

Very simple application that is composed of:

- [**Backend**] Python Flask worker application that auto sets a starting number, summing up all numbers in the hostname (ie: e1a7734fa926 => 39), and starting a auto counter every random seconds, it also records the amount of unique visits each container has (even though they are loadbalanced)

!!! info
    When you click on the button or visit the Ingress URL for the micro app it will return a json file with the described information.

- [**Frontend**] Python Flask application that displays data retrieved from redis for each container (Pod) worker; visits per container (even if they are load balanced) and auto counter.

- [**Database**] Redis container that stores the data from the worker application



## Example:


| Container Hostname      | Auto Counter | Unique visits     |
| :---        |    :----:   |          ---: |
| e1a7734fa926      | 106       | 3  |
| 8b2fc77c9f47   | 95        | 5      |
| b2f7434fa874      | 16       | 1  |
| 9a0fc42c1d32   | 32        | 0      |



